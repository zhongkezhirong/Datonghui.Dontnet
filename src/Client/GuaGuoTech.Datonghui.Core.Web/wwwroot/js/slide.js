﻿
////滑动对象
var mySlide = function (o) {
    this.config = o;
    var obj = this;
    ///触摸开始的坐标
    this.sPoint = {};
    //移动坐标
    this.mPoint = {};
       
    this.carousel = o.carouselId;


    ///事件绑定到prototype上
    this.config.bind.addEventListener("touchstart", function (e) {
     return    obj.start(e);
    }, false);

    this.config.bind.addEventListener("touchmove", function (e) {

        return obj.move(e);
    }, false);

    this.config.bind.addEventListener("touchend", function (e) {
        return obj.end(e);

    }, false);

}

///touch事件开始
mySlide.prototype.start = function (e) {

    var point = e.touches ? e.touches[0] : e.touches;
    this.sPoint.x = point.screenX;
    this.sPoint.y = point.screenY;
}

mySlide.prototype.move = function (e) {
    var point = e.touches ? e.touches[0] : e.touches;
    this.mPoint.x = point.screenX;
    this.mPoint.y = point.screenY;

}

//touch 事件结束
mySlide.prototype.end = function (e) {
    var num = this.sPoint.x - this.mPoint.x;

    if (num > 0) {
        $(this.config.bind).carousel('next');
    }
    else {
        $(this.config.bind).carousel('prev');
    }
    //this.config.backFunction();

}

